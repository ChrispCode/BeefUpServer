module.exports = (connection, DataTypes) => {
    const Reservation = connection.define('reservation', {
        date: {
            type: DataTypes.DATE,
            allowNull: false
        },
        duration: {
            type: DataTypes.INTEGER,
            defaultValue: 120,
            validate: {
                min: 10,
                max: 300
            }
        }
    }, {
        underscored: true,
        freezeTableName: true,
        classMethods: {
            associate: (models) => {
                Reservation.belongsTo(models.user, {as: 'Client'});
                Reservation.belongsTo(models.table);
            }
        }
    });

    return Reservation;
};