module.exports = (connection, DataTypes) => {
    const Token = connection.define('token', {
        jwt: {
            type: DataTypes.STRING,
            allowNull: false,
            notEmpty: true
        },
        device: {
            type: DataTypes.STRING,
            allowNull: false,
            notEmpty: true
        }
    }, {
        underscored: true,
        freezeTableName: true,
        classMethods: {
            associate: (models) => {
                Token.belongsTo(models.user);
            }
        }
    });

    return Token;
};