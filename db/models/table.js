module.exports = (connection, DataTypes) => {
    const Table = connection.define('table', {
        number_of_people: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                min: 2
            }
        }
    }, {
        underscored: true,
        freezeTableName: true,
        classMethods: {
            associate: (models) => {
                Table.belongsTo(models.hall);
                Table.belongsTo(models.user, {as: 'Client'});
                Table.hasMany(models.reservation);
            }
        }
    });

    return Table;
};