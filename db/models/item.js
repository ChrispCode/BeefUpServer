module.exports = (connection, DataTypes) => {
    const Item = connection.define('item', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            notEmpty: true
        },
        time_to_prepare: {
            type: DataTypes.INTEGER,
            validate: {
                min: 1
            }
        },
        ingredients: {
            type: DataTypes.ARRAY(DataTypes.STRING),
            notEmpty: true
        },
        liquid: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        weight: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                min: 1
            }
        },
        price: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                min: 1
            }
        }
    }, {
        underscored: true,
        freezeTableName: true,
        classMethods: {
            associate: function (models) {
                Item.belongsTo(models.restaurant);
                Item.belongsToMany(models.order, { through: models.order_item });
            }
        }
    });

    return Item;
};