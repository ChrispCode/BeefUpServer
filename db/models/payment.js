module.exports = (connection, DataTypes) => {
    const Payment = connection.define('payment', {
        stripe_charge_id: {
            type: DataTypes.STRING,
            notEmpty: true
        },
        amount: {
            type: DataTypes.INTEGER,
            validate: {
                min: 1
            }
        },
        repaid: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    }, {
        underscored: true,
        freezeTableName: true,
        classMethods: {
            associate: (models) => {
                Payment.belongsTo(models.order);
                Payment.belongsTo(models.restaurant);
            }
        }
    });

    return Payment;
};