module.exports = (connection, DataTypes) => {
    const OrderItem = connection.define('order_item', {
        quantity: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1,
            validate: {
                min: 1
            }
        }
    }, {
        underscored: true,
        freezeTableName: true
    });

    return OrderItem;
};