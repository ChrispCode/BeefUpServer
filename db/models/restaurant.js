const geocoder = require('../../config/geocoder');
const { InvalidAddressError } = require('../../graphql/errors');

module.exports = (connection, DataTypes) => {
    const Restaurant = connection.define('restaurant', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            notEmpty: true
        },
        address: {
            type: DataTypes.VIRTUAL
        },
        latitude: {
            type: DataTypes.DOUBLE,
            validate: {
                min: -90,
                max: 90
            }
        },
        longitude: {
            type: DataTypes.DOUBLE,
            validate: {
                min: -180,
                max: 180
            }
        },
        country: {
            type: DataTypes.STRING,
            notEmpty: true
        },
        city: {
            type: DataTypes.STRING,
            notEmpty: true
        },
        street_name: {
            type: DataTypes.STRING,
                notEmpty: true
        },
        street_number: {
            type: DataTypes.INTEGER,
            notEmpty: true,
            validate: {
                min: 1
            }
        }
    }, {
        underscored: true,
        freezeTableName: true,
        classMethods: {
            associate: (models) => {
                Restaurant.hasMany(models.item);
                Restaurant.hasMany(models.hall);
                Restaurant.hasMany(models.payment);
                Restaurant.belongsTo(models.user, { as: 'Owner', foreignKey: 'owner_id' });
                Restaurant.hasMany(models.user, {
                    as: 'Clients',
                    constraints: false,
                    defaultScope: {
                        where: {
                            restaurant_id: this.id,
                            role: 'normal'
                        }
                    }
                });
                Restaurant.hasMany(models.user, {
                    as: 'Staff',
                    constraints: false,
                    defaultScope: {
                        where: {
                            restaurant_id: this.id,
                            role: {
                                in: ['waiter', 'barman', 'cook', 'cashier']
                            }
                        }
                    }
                });
            }
        },
        hooks: {
            beforeCreate: (restaurant, options) => new Promise((resolve, reject) => {
                return resolve(verifyAddress(restaurant));
            }),
            beforeUpdate: (restaurant, options) => new Promise((resolve, reject) => {
                if (restaurant.changed('address'))
                    return resolve(verifyAddress(restaurant));

                return resolve();
            })
        },
    });

    return Restaurant;
};

const verifyAddress = restaurant => new Promise((resolve, reject) => {
    geocoder.geocode(restaurant.address)
        .then(location => {
            if (location.length == 0)
                return reject(new InvalidAddressError);

            location = location[0];
            console.log(location)
            restaurant = Object.assign(restaurant, {
                latitude: location.latitude,
                longitude: location.longitude,
                country: location.country,
                city: location.city,
                street_name: location.streetName || null,
                street_number: location.streetNumber || null
            });
            return resolve();
        })
        .catch(err => {
            return reject(err);
        })
});
