module.exports = (connection, DataTypes) => {
    const Order = connection.define('order', {
        payed: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    }, {
        underscored: true,
        freezeTableName: true,
        classMethods: {
            associate: (models) => {
                Order.belongsTo(models.user, {as: 'Client'});
                Order.belongsToMany(models.item, { through: 'order_item' });
                Order.hasOne(models.payment);
            }
        }
    });

    return Order;
};