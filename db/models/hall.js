module.exports = (connection, DataTypes) => {
    const Hall = connection.define('hall', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            notEmpty: true
        }
    }, {
        underscored: true,
        freezeTableName: true,
        classMethods: {
            associate: (models) => {
                Hall.hasMany(models.table);
                Hall.belongsTo(models.restaurant);
            }
        }
    });

    return Hall;
};