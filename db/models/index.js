var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
var config = require('../../config');

const connection = new Sequelize(
  config.db.name,
  config.db.user,
  config.db.password,
  config.db.options
);
var db = {};

fs.readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
  })
  .forEach(function(file) {
    var model = connection.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.connection = connection;
db.Sequelize = Sequelize;

module.exports = db;