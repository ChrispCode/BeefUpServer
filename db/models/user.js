const bcrypt = require('bcrypt');

const hashPassword = user => new Promise((resolve, reject) => {
    user.hashPassword(user.password, user.hash)
        .then(hashedPassword => {
            user.password = hashedPassword;
            return resolve();
        })
        .catch(err => {
            return reject(err);
        });
});

module.exports = (connection, DataTypes) => {
    const User = connection.define('user', {
        username: {
            type: DataTypes.STRING,
            allowNull: false,
            notEmpty: true,
            unique: {
                msg: 'There is a user with this username'
            }
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            notEmpty: true,
            unique: {
                msg: 'There is a user with this email'
            },
            validate: {
                isEmail: true
            }
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
            notEmpty: true
        },
        first_name: {
            type: DataTypes.STRING,
            allowNull: false,
            notEmpty: true
        },
        last_name: {
            type: DataTypes.STRING,
            allowNull: false,
            notEmpty: true
        },
        role: {
            type: DataTypes.ENUM,
            values: ['normal', 'waiter', 'barman', 'cook', 'cashier', 'owner'],
            defaultValue: 'normal'
        },
        stripe_customer_id: {
            type: DataTypes.STRING,
            notEmpty: true
        },
        stripe_account_id: {
            type: DataTypes.STRING,
            notEmpty: true
        }
    }, {
        underscored: true,
        freezeTableName: true,
        classMethods: {
            associate: function (models) {
                User.hasMany(models.token);
                User.hasOne(models.table, {foreignKey: 'client_id'});
                User.hasMany(models.order, {foreignKey: 'client_id'});
                User.hasMany(models.reservation, {foreignKey: 'client_id'});
                User.hasOne(models.restaurant, { as: 'Establishment', foreignKey: 'owner_id' });
                User.belongsTo(models.restaurant, {
                    constraints: false
                });
            }
        },
        hooks: {
            beforeCreate: (user, options) => new Promise((resolve, reject) => {
                return resolve(hashPassword(user));
            }),
            beforeUpdate: (user, options) => new Promise((resolve, reject) => {
                if (user.changed('password'))
                    return resolve(hashPassword(user));

                return resolve();
            })
        },
        instanceMethods: {
            verifyPassword: function(password) {
                return new Promise((resolve, reject) => {
                    return resolve(bcrypt.compare(password, this.password));
                })
            },
            getEmployer: function (connectors) {
                return new Promise((resolve, reject) => {
                    connection.models.restaurant.findById(this.restaurant_id)
                        .then(restaurant => {
                            return restaurant.getOwner();
                        })
                        .then(owner => {
                            return resolve(owner);
                        })
                        .catch(err => {
                            return reject(err);
                        });
                });
            }
        }
    });

    const hashPassword = user => new Promise((resolve, reject) => {
        bcrypt.hash(user.password, 10)
            .then(hash => {
                user.password = hash;
                return resolve();
            })
            .catch(err => {
                return reject(err);
            });
    });

    return User;
};