require('dotenv').config();
const models = require('../models');

models.connection.sync({ force: true }).then(() => { 
    let createdRestaurant;
    let createdTable;
    let createdUser;
    models.user.create({ 
        username: 'emil12543', 
        email: 'emil12542@gmail.com', 
        password: '123456', 
        first_name: 'Emil', 
        last_name: 'Kostadinov' 
    }).then(user => {
        createdUser = user;
        return user.createEstablishment({
            name: 'Rest 1', 
            address: 'bulgaria, blagoevgrad, spartak 1'
        });
    }).then(restaurant => {
        createdRestaurant = restaurant; 
        return restaurant.createItem({ 
            name: 'sth', 
            time_to_prepare: 1, 
            ingredients: ['1', '2'], 
            weight: 100, 
            price: 100
        }) 
    }).then(order => {
        return createdRestaurant.createHall({
            name: 'terasa'
        });
    }).then(hall => {
        return hall.createTable({
            number_of_people: 4
        });
    }).then(table => {
        createdTable = table;
        return table.createReservation({
            client_id: 1,
            date: 1486326062593
        });
    }).then(reservation => {
        return createdUser.createOrder();
    }).then(order => {
        return order.addItem(1);
    })
    .catch(err => {
        throw err;
    });
});