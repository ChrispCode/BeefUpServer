# BeefUp Ecosystem
The BeefUp Ecosystem contains of cross-platform desktop and mobile applications and a Graphql server. 

## BeefUp Server - The backend
The backend is cleverly designed in mind of the industry standards for creating servers that will be used with multiple applications on different platforms. Big corporations like Facebook, Github, Wallmart are using there technologies to run their businesses.

## BeefUp Server - Technologies based uppon
> [Hapi.js](https://hapijs.com/) - The most stable node.js server framework 

> [PostgreSQL](https://www.postgresql.org/) - The most stable node.js server framework

> [Graphql](http://graphql.org/) - The industry standart for building robust APIs and querying easily data, while getting the data that you only want

> [Apollo](http://www.apollodata.com/) - Managing tools and product for Graphql

> [Sequelize](http://docs.sequelizejs.com/en/v3/) - The best JavaScript ORM

## BeefUp Server - Steps to run
**Please always run the project on the latest version, which can be found on the master branch in this repo:**

	https://gitlab.com/ChrispCode/BeefUpServer

When you make sure you have [Git](https://git-scm.com/downloads) installed you can open a command promp(shell, bash) and clone the repo, then navigate to it:

	git clone https://gitlab.com/ChrispCode/BeefUpServer
	cd BeefUpServer

When you have the code to your machine and you have navigated to it, you can continue. Make sure you have [Node and NPM](https://nodejs.org/en/) installed, as well as you have a local installation of [PostgreSQL](https://www.postgresql.org/). Then run: 

	npm install

The next thing you have to do is to create an `.env` file where all the environment variables will be stored. Good thing we have you backed up with an example file.

	cp .env.example .env

**Check every variable of the newly created `.env` file. If something in your databese configuration is different change it!**
When you are ready with the `.env` file you can put some bootstrap data in the database and the run the project like so:

	npm run bootstrap
	npm start

A local server wiil run at port `8000`. You can traverse or mutate the data in the database with the help of `Graphiql`. We advise you to take a look at:

	http://localhost:8000/graphiql
### That is all for the BeefUp Server!

***
## BeefUp Desktop - The administration
The administration is a desktop application built under web technologies, mainly JS(ES6) and then transpiled with the help of [Babel](https://babeljs.io/) and then compiled with [Electron](https://electron.atom.io/). Other tools include: 

> [React](https://facebook.github.io/react/) - Industry leading front-end single page library

> [Redux](http://redux.js.org/) - State management tool

> [Webpack](https://webpack.js.org/) - Module asset bundler

> [Apollo](http://www.apollodata.com/) - Tools and products for Graphql

> [Electron-Builder](https://www.npmjs.com/package/electron-builder) - Tools for compiling electron app to cross-platform installers

## BeefUp Desktop - Steps to run

**Please always run the project on the latest version, which can be found on the master branch in this repo:**

	https://gitlab.com/ChrispCode/BeefUpDesktop

As the server, here you will aslo need to have [Git](https://git-scm.com/downloads), as well as [Node and NPM](https://nodejs.org/en/). Open the terminal in the location where you what to have the project to. With the following 3 command you will download the project to your desktop, enter into it and then install all the needed dependencies in order to run it.

	git clone https://gitlab.com/ChrispCode/BeefUpDesktop
	cd BeefUpDesktop
	npm install

When the installation of the dependencies is finished you can start the project in development by running: 

	npm run dev

Or start it in production by running: 

	npm run build && npm start

You can also compile a build an executable by running:

	npm run build
	npm run package-win // if bulding for windows
	OR
	npm run package-linux // if building for linux

## BeefUp Mobile - The client and staff tool
The BeefUp mobile app is the tool that is going to do most of the heavy work, when it comes to interaction of the restaurant clients, as well as it will be the application that the waiters will be using.

> [React-Native](https://facebook.github.io/react-native/) - The ability to write JS code once and compile to IOS and Android

> [Exponent](https://expo.io/) - Easy compilation of React-Native

> [Apollo](https://webpack.js.org/) - Tools and products for Graphql


## BeefUp Mobile - Steps to run

**Please always run the project on the latest version, which can be found on the master branch in this repo:**

	https://gitlab.com/ChrispCode/BeefUpMobile

As the server and desktop application, here you will aslo need to have [Git](https://git-scm.com/downloads), as well as [Node and NPM](https://nodejs.org/en/). After that make sure you have the exponent app installed at your smartphone. You can find the app [here](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=en).
After you install the app you can clone the source to your machine by:

	git clone https://gitlab.com/ChrispCode/BeefUpMobile
	cd BeefUpMobile
	npm install

Now you need to install exponent to your computer as a global npm dependency. This can be done with the command: 

    npm install -g exp

Finally to run the application you have to execute these command in the terminal;

    configure exp file
    exp start
