require('dotenv').config();

const config = require('./config');
const Hapi = require('hapi');
const server = new Hapi.Server();
const JWT = require('jsonwebtoken');
const plugins = require('./plugins');
const pusher = require('./config/pusher');

server.connection({
    port: config.server.port,
    routes: {
        cors: true
    }
});

server.ext('onRequest', (request, reply) => {
    const auth = request.headers['authorization'];
    if (auth && auth !== null) {
        return JWT.verify(auth, config.secret, (err, decoded) => {
            if (err)
                return reply({
                    message: 'Something went wrong with the authorization token'
                });

            request.auth = decoded;
            return reply.continue();
        });
    }

    return reply.continue();
});

server.route({
    method: 'POST',
    path: '/pusher/auth',
    handler: (request, reply) => {
        if (typeof request.headers['authorization'] === 'undefined')
            return reply().code(403);

        let socketId = request.payload.socket_id;
        let channel =request.payload.channel_name;
        let auth = pusher.authenticate(socketId, channel);
        return reply(auth);
    }
});

server.register(plugins, err => {
    if (err)
        throw err;
});

server.start(err => {
    if (err)
        throw err;

    console.log(`Server running at: ${server.info.uri}`);
});

module.exports = server;