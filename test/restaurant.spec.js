const expect = require('chai').expect;
const request = require('./request');
const server = require('../server');

describe('Restaurant Queries and Mutations', () => {
    it('should get restaurant by id', done => {
        server.inject(request(`
            {
                restaurant(id: 1) {
                    id
                }
            }
        `), response => {
            const data = JSON.parse(response.result).data;
            expect(data.restaurant.id).to.equal(1);
            done();
        });
    });

    // it.only('should get a token', done => {
    //     server.inject(request(`
    //         mutation {
    //             createToken(username: "emil12543", password: "emil12543")
    //         }
    //     `), response => {
    //         const data = JSON.parse(response.result).data;
    //         console.log(data);
    //         done()
    //     });
    // });

    // it('should get restaurant by name');
    it.only('should get user orders', done => {
        server.inject(request(`
            mutation {
                updateUser(password_confirmation: "123456", first_name: "new barman name", id: 2) {
                    username
                    first_name
                }
            }
        `, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwicm9sZSI6Im93bmVyIiwiaWF0IjoxNDg2NTU4OTM4fQ.IRBjO5D0b12VFUCJePlqWzPtumjsbtOvqcSx_0xBhxA'),
            response => {
                const data = JSON.parse(response.result).data;
                console.log(JSON.stringify(data));
                done();
            });
    })
});

// "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwicm9sZSI6Im5vcm1hbCIsImlhdCI6MTQ4NjQ4MzQ5Nn0.hulfYVellExdP8E5c8osepYYeixivwbS6MffLapZovQ"