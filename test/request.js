module.exports = (query, auth) => {
    return {
        method: 'POST',
        url: '/graphql',
        payload: {
            query
        },
        headers: {
            'Authorization' : auth
        }
    }
};