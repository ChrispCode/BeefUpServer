const async = require('async');
const { UnauthorizedError, SecurityUnauthorizedArgumentError, securityMissingArgumentError, securityInvalidArgumentError } = require('../errors');

class Security {
    static isAuthorized(auth, rules) {
        if (typeof rules === 'undefined')
            return true;
        if (rules.length == 1 && rules[0] === null)
            return !auth;
        if (rules.indexOf(null) > -1)
            return !!auth || rules.indexOf(auth.role) > -1;
        if (rules[0] === '*')
            return !!auth;

        return rules.indexOf(auth.role) > -1;
    }

    static isUndefined(data) {
        return typeof data === 'undefined';
    }

    static isNull(data) {
        return data === null;
    }

    static checkAuth(rules, auth) {
        // rules is undefined if only unauthorized users can call it
        // rules == * if only authorized users can call it
        // rules is array if specific users can call it
        auth = auth.role;

        if (Array.isArray(rules))
            return rules.indexOf(null) > -1 ? rules.indexOf(auth) > -1 || !auth : rules.indexOf(auth) > -1;
        else if (rules === '*')
            return !!auth;
        else
            return !auth;
    }
}

module.exports = Security;