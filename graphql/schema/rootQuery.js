const Types = require('../index').types;

const RootQuery = `
    type RootQuery {
        restaurants: [Restaurant]
        restaurant(id: Int!): Restaurant
        user(id: Int): User
    }
`;

module.exports = () => [RootQuery, ...Types()];
