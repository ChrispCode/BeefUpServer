const RootQuery = require('./rootQuery');
const Mutation = require('./rootMutation');
const Resolvers = require('../index').resolvers;
const {makeExecutableSchema} = require('graphql-tools');

const SchemaDefinition = `
  schema {  
    query: RootQuery,
    mutation: Mutation
  }
`;

module.exports = makeExecutableSchema({
    typeDefs: [SchemaDefinition, RootQuery, Mutation],
    resolvers: Resolvers
});