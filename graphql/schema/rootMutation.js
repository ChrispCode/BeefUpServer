const Mutation = `
    type Mutation {
        # User mutations
        createUser(username: String!, password: String!, email: String!, first_name: String!, last_name: String!, role: UserRole, restaurant_id: Int, stripe_token: String): User
        createToken(username: String!, password: String!): UserToken
        updateUser(id: Int, first_name: String, last_name: String, email: String, password: String, password_confirmation: String!): User
        quitRestaurant: Boolean
        
        # Restaurant mutations
        createRestaurant(name: String!, address: String!): Restaurant
        updateRestaurant(id: Int!, name: String, address: String): Restaurant
        removeRestaurant(id: Int!): Restaurant
        authorizeClient(client_id: Int!, number_of_people: Int, duration: Int): Table
        # withrawBalanceRestaurant(id: Int!, startDate: Date, endDate: Date): Restaurant
        
        # Hall mutations
        createHall(restaurant_id: Int!, name: String!): Hall
        updateHall(id: Int!, name: String): Hall
        removeHall(id: Int!): Hall
        
        # Table mutations
        createTable(hall_id: Int!, number_of_people: Int): Table
        updateTable(id: Int!, number_of_people: Int): Table
        removeTable(id: Int): Table
        
        # Item mutations
        createItem(restaurant_id: Int!, name: String!, time_to_prepare: Int!, ingredients: [String]!, liquid: Boolean!, weight: Int!, price: Int!): Item
        updateItem(id: Int!, name: String, time_to_prepare: Int, ingredients: [String], liquid: Boolean, weight: Int, price: Int): Item
        removeItem(id: Int!): Item
        
        # Order mutations
        createOrder(items: [OrderItemInput]!): Order
        payOrder(id: Int!, payment_method: PaymentMethods): Order
        
        # Reservation mutations
        createReservation(table_id: Int!, date: Date!, duration: Int): Reservation
        removeReservation(id: Int!): Reservation
    }
`;

module.exports = () => [Mutation];