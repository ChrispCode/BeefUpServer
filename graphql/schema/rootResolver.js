const User = require('../helpers/user');
const Restaurant = require('../helpers/restaurant');

const resolve = {
    RootQuery: {
        restaurant(_, args, { connectors, auth }) {
            return Restaurant.get(args, connectors, auth);
        },
        restaurants(_, args, { connectors, auth }) {
            return Restaurant.get(args, connectors, auth);
        },
        user(_, args, { connectors, auth }) {
            return User.get(args, connectors, auth);
        }
    }
};

module.exports = resolve;