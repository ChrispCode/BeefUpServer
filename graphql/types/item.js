const Item = `
    type Item {
      id: Int
      name: String
      time_to_prepare: Int
      ingredients: [String]
      liquid: Boolean
      weight: Int
      price: Int
    }
`;

module.exports = () => [Item];