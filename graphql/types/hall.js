const Hall = `
    type Hall {
      id: ID
      name: String
      restaurant: Restaurant
      tables: [Table]
    }
`;

module.exports = () => [Hall];