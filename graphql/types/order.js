const Order = `
    input OrderItemInput {
        item_id: Int!
        quantity: Int
    }

    type OrderItem {
        item: Item
        quantity: Int
    }
    
    enum PaymentMethods {
        cash
        card
    }

    type Order {
      id: Int
      payed: Boolean
      items: [OrderItem]
      client: User
      table: Table
    }
`;

module.exports = () => [Order];