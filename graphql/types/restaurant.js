const Restaurant = `
    type Restaurant {
      id: ID
      name: String
      latitude: Float
      longitude: Float
      country: String
      city: String
      street_name: String
      street_number: Int
      balance: Int
      clients: [User]
      staff: [User]
      halls: [Hall]
      items: [Item]
      orders: [Order]
    }
`;

module.exports = () => [Restaurant];