const Table = `
    type Table {
      id: ID
      number_of_people: Int
      hall: Hall
      clients: [User]
      orders: [Order]
      reservations: [Reservation]
    }
`;

module.exports = () => [Table];