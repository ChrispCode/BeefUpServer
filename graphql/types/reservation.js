const Reservation = `
    type Reservation {
      id: Int
      client: User
      table: Table
      date: Date
    }
`;

module.exports = () => [Reservation];