const User = `
    type User {
      id: Int
      username: String
      email: String
      first_name: String
      last_name: String
      role: UserRole
      orders: [Order]
    }
    
    type UserToken {
        token: String
        role: UserRole
        id: Int
        restaurant_id: Int
    }
    
    enum UserRole {
        normal
        waiter
        barman
        cook
        cashier
        owner
    }
`;

module.exports = () => [User];