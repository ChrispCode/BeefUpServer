const ExtendableError = require('es6-error');

class SecurityMissingArgumentError extends ExtendableError {
    constructor(argument) {
        // super(message || 'Argument ' + parentArgument.name + ' should be followed by argument ' + argument.name);
        super('Argument ' + argument + ' is missing')
    }
}

module.exports = SecurityMissingArgumentError;