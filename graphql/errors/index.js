const fs = require('fs');
const path = require('path');

const errors = {};

fs.readdirSync(__dirname)
    .filter(file => (file.indexOf('.') !== 0) && (file !== 'index.js'))
    .map(file => {
        const errorFile = require(path.join(__dirname, file));
        errors[errorFile.name] = errorFile;
    });

module.exports = errors;