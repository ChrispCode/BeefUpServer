const ExtendableError = require('es6-error');

class InvalidAddressError extends ExtendableError {
    constructor() {
        super('The provided address is wrong');
    }
}

module.exports = InvalidAddressError;