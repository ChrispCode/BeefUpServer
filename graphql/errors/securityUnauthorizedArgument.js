const ExtendableError = require('es6-error');

class SecurityUnauthorizedArgumentError extends ExtendableError {
    constructor(argument) {
        super('You are not authorized to use ' + argument);
    }
}

module.exports = SecurityUnauthorizedArgumentError;