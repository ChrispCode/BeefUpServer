const ExtendableError = require('es6-error');

class SecurityInvalidArgumentError extends ExtendableError {
    constructor(argument) {
        super('Argument ' + argument.name + ' is invalid');
    }
}

module.exports = SecurityInvalidArgumentError;