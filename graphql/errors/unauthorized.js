const ExtendableError = require('es6-error');

class UnauthorizedError extends ExtendableError {
    constructor(message = 'You are not authorized for this action') {
        super(message);
    }
}

module.exports = UnauthorizedError;