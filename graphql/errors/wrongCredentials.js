const ExtendableError = require('es6-error');

class WrongCredentialsError extends ExtendableError {
    constructor(message = 'Wrong credentials') {
        super(message);
    }
}

module.exports = WrongCredentialsError;