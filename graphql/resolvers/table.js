const Table = require('../helpers/table');

const resolve = {
    Table: {
        hall({ id }, args, { connectors }) {
            return connectors.table.getHalls.load(id);
        },
        clients({ id }, args, { connectors, auth }) {
            // return connectors.table.getClients.load(id);
            args.id = id;
            return Table.getClients(args, connectors, auth);
        },
        orders({ id }, args, { connectors, auth }) {
            args.id = id;
            return Table.getOrders(args, connectors, auth);
        },
        reservations({ id }, args, { connectors }) {
            return connectors.table.load(id).then(table => {
                return table.getReservations();
            });
        }
    },
    Mutation: {
        createTable(_, args, { connectors, auth }) {
            return Table.create(args, connectors, auth);
        },
        updateTable(_, args, { connectors, auth }) {
            return Table.update(args, connectors, auth);
        },
        removeTable(_, args, { connectors, auth }) {
            return Table.remove(args, connectors, auth);
        }
    }
};

module.exports = resolve;

