const Reservation = require('../helpers/reservation');

const resolve = {
    Reservation: {
        client({ id }, args, { connectors }) {
            return connectors.reservation.load(id).then(reservation => {
                return reservation.getClient();
            });
        },
        table({ id }, args, { connectors }) {
            return connectors.reservation.load(id).then(reservation => {
                return reservation.getTable();
            });
        }
    },
    Mutation: {
        createReservation(_, args, { connectors, auth }) {
            return Reservation.create(args, connectors, auth);
        },
        removeReservation(_, args, { connectors, auth }) {
            return Reservation.remove(args, connectors, auth);
        }
    }
};

module.exports = resolve;