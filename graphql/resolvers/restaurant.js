const Restaurant = require('../helpers/restaurant');

const resolve = {
    Restaurant: {
        balance({ id }, args, { connectors, auth }) {
            args.id = id;
            return Restaurant.getBalance(args, connectors, auth);
        },
        clients({ id }, args, { connectors, auth }) {
            args.id = id;
            return Restaurant.getClients(args, connectors, auth);
        },
        halls({ id }, args, { connectors }) {
            return connectors.restaurant.getHalls.load(id);
        },
        items({ id }, args, { connectors }) {
            return connectors.restaurant.getItems.load(id);
        },
        staff({ id }, args, { connectors, auth }) {
            args.id = id;
            return Restaurant.getStaff(args, connectors, auth);
        },
        orders({ id }, args, { connectors, auth }) {
            args.id = id;
            return Restaurant.getOrders(args, connectors, auth);
        }
    },
    Mutation: {
        createRestaurant(_, args, { connectors, auth }) {
            return Restaurant.create(args, connectors, auth);
        },
        updateRestaurant(_, args, { connectors, auth }) {
            return Restaurant.update(args, connectors, auth);
        },
        removeRestaurant(_, args, { connectors, auth }) {
            return Restaurant.remove(args, connectors, auth);
        },
        authorizeClient(_, args, { connectors, auth }) {
            return Restaurant.authorizeClient(args, connectors, auth);
        }
        // withrawBalanceRestaurant(_, args, { connectors, auth }) {
        //     return Restaurant.withrawBalance(args, connectors, auth);
        // }
    }
};

module.exports = resolve;

