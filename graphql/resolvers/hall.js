const Hall = require('../helpers/hall');

const resolve = {
    Hall: {
        restaurant({ id }, args, { connectors }) {
            return connectors.hall.getRestaurant.load(id);
        },
        tables({ id }, args, { connectors }) {
            return connectors.hall.getTables.load(id);
        }
    },
    Mutation: {
        createHall(_, args, { connectors, auth }) {
            return Hall.create(args, connectors, auth);
        },
        updateHall(_, args, { connectors, auth }) {
            return Hall.update(args, connectors, auth);
        },
        removeHall(_, args, { connectors, auth }) {
            return Hall.remove(args, connectors, auth);
        }
    }
};

module.exports = resolve;

