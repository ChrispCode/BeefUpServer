const User = require('../helpers/user');

const resolve = {
    User: {
        orders({ id }, args, { connectors, auth }) {
            args.id = id;
            return User.getOrders(args, connectors, auth);
        }
    },
    Mutation: {
        createUser(_, args, { connectors, auth }) {
            return User.create(args, connectors, auth);
        },
        createToken(_, args, { connectors, auth }) {
            return User.createToken(args, connectors, auth);
        },
        updateUser(_, args, { connectors, auth }) {
            return User.update(args, connectors, auth);
        }
    }
};

module.exports = resolve;
