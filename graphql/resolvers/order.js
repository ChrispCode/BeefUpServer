const Order = require('../helpers/order');

const resolve = {
    Order: {
        client({ id }, args, { connectors, auth }) {
            return connectors.order.getClients.load(id);
        },
        table({ id }, args, { connectors }) {
            return connectors.order.getTables.load(id);
        },
        items({ id }, args, { connectors }) {
            return connectors.order.getItems.load(id);
        }
    },
    OrderItem: {
        item({ item_id }, args, { connectors }) {
            return connectors.item.find.load(item_id);
        }
    },
    Mutation: {
        createOrder(_, args, { connectors, auth }) {
            return Order.create(args, connectors, auth);
        },
        payOrder(_, args, { connectors, auth }) {
            return Order.pay(args, connectors, auth);
        }
    }
};

module.exports = resolve;

