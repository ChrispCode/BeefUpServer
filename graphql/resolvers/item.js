const Item = require('../helpers/item');

const resolve = {
    Mutation: {
        createItem(_, args, { connectors, auth }) {
            return Item.create(args, connectors, auth);
        },
        updateItem(_, args, { connectors, auth }) {
            return Item.update(args, connectors, auth);
        },
        removeItem(_, args, { connectors, auth }) {
            return Item.remove(args, connectors, auth);
        }
    }
};

module.exports = resolve;