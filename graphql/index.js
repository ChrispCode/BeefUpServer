const fs = require('fs');
const path = require('path');
const DataLoader = require('dataloader');
const merge = require('lodash.merge');
const models = require('../db/models');

let data = {
    types: [],
    resolvers: {},
    connectors: {}
};

const getFolder = p => {
    return path.basename(p);
};

const finder = (directory) => {
    fs.readdirSync(directory)
        .map(file => {
            if (!fs.statSync(path.join(directory, file)).isDirectory()) {
                if (getFolder(directory) === 'types' || (path.basename(path.join(directory, '..')) === 'types' && getFolder(directory) === 'scalars')) {
                    data.types.push(require(path.join(directory, file)));
                    if (getFolder(directory) === 'types')
                        data.connectors[file.replace(/\.[^/.]+$/, '')] = null;
                } else
                    data.resolvers = merge(data.resolvers, require(path.join(directory, file)));
            } else if (file === 'scalars')
                finder(path.join(directory, file), 'scalars');
        });
};

fs.readdirSync(__dirname)
    .filter(file => fs.statSync(path.join(__dirname, file)).isDirectory() && ['types', 'resolvers'].indexOf(file) > -1)
    .map(file => {
        finder(path.join(__dirname, file));
    });

data.resolvers = merge(data.resolvers, require(path.join(__dirname, 'schema', 'rootResolver.js')));

module.exports = {
    types: () => data.types,
    resolvers: data.resolvers,
    connectors: () => {
        let connectors = {};
        for (let connector in data.connectors) {
            connectors[connector] = new DataLoader(ids => {
                return new Promise((resolve, reject) => {
                    models[connector].findAll({
                        where: {
                            id: ids
                        }
                    }).then(items => {
                        if (items.length === 0)
                            return reject(new Error('There is no such ' + connector));

                        return resolve(items);
                    }).catch(err => {
                        return reject(err);
                    });
                });
            });
        }
        return connectors;
    }
};