const async = require('async');

const models = require('../../db/models');
const Security = require('../security');
const Restaurant = require('./restaurant');

const { UnauthorizedError, SecurityUnauthorizedArgumentError, SecurityMissingArgumentError, SecurityInvalidArgumentError, WrongCredentialsError } = require('../errors');

class Hall {
    static create(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Restaurant.checkOwner({ id: args.restaurant_id }, connectors, auth)
                        .then(() => {
                            return resolve(models.hall.create(Object.assign(args)));
                        })
                        .catch(err => {
                            return callback(err);
                        })
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    static checkOwner(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            connectors.hall.find.load(args.id)
                .then(hall => {
                    return resolve(Restaurant.checkOwner({id: hall.restaurant_id}, connectors, auth));
                })
                .catch(err => {
                    return reject(err);
                })
        });
    }

    static update(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Hall.checkOwner(args, connectors, auth)
                        .then(() => {
                            return connectors.hall.find.load(args.id);
                        })
                        .then(hall => {
                            delete args['restaurant_id'];
                            return resolve(hall.update(args));
                        })
                        .catch(err => {
                            return callback(err);
                        });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    static remove(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Hall.checkOwner(args, connectors, auth)
                        .then(() => {
                            return connectors.hall.find.load(args.id);
                        })
                        .then(hall => {
                            return resolve(hall.destroy());
                        })
                        .catch(err => {
                            return callback(err);
                        });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }
}

module.exports = Hall;