const JWT = require('jsonwebtoken');
const async = require('async');

const models = require('../../db/models');
const config = require('../../config');
const stripe = require('../../config/stripe');
const Security = require('../security');
const { UnauthorizedError, SecurityUnauthorizedArgumentError, SecurityMissingArgumentError, SecurityInvalidArgumentError, WrongCredentialsError } = require('../errors');

class User {
    static create(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth([null, 'owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    if (auth.role === 'owner') {
                        if (Security.isUndefined(args.restaurant_id))
                            return callback(new SecurityMissingArgumentError('restaurant_id'));
                        if (Security.isUndefined(args.role))
                            return callback(new SecurityMissingArgumentError('role'));

                        connectors.restaurant.find.load(args.restaurant_id)
                            .then(restaurant => {
                                if (restaurant.owner_id !== auth.id)
                                    return callback(new UnauthorizedError);

                                return callback();
                            })
                            .catch(err => {
                                return callback(err)
                            })
                    } else {
                        if (!Security.isUndefined(args.restaurant_id))
                            return callback(new SecurityUnauthorizedArgumentError('restaurant_id'));
                        if (args.role !== 'owner' && !Security.isUndefined(args.role))
                            return callback(new SecurityUnauthorizedArgumentError('role'));

                        return callback();
                    }
                },
                callback => {
                    if (args.role === 'owner') {
                        if (Security.isUndefined(args.stripe_token))
                            return callback(new SecurityMissingArgumentError('stripe_token'));

                        let ownerStripeCustomer;
                        let ownerStripeAccount;

                        stripe.customers.create({
                            email: args.email,
                            source: args.stripe_token
                        }).then(customer => {
                            ownerStripeCustomer = customer;
                            return stripe.account.create({
                                country: 'US',
                                email: args.email
                            });
                        }).then(account => {
                            ownerStripeAccount = account;
                            return stripe.charges.create({
                                amount: 20000,
                                currency: 'bgn',
                                customer: ownerStripeCustomer.id
                            });
                        }).then(charge => {
                            args.stripe_customer_id = ownerStripeCustomer.id;
                            args.stripe_account_id = ownerStripeAccount.id;
                            return callback();
                        }).catch(err => {
                            return callback(err);
                        });
                    } else
                        return callback();
                },
                callback => {
                    return resolve(models.user.create(args));
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    static createToken(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(null, auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    let jwtData;
                    models.user.findOne({
                        where: {
                            username: args.username
                        }
                    }).then(user => {
                        if (Security.isNull(user))
                            return callback(new WrongCredentialsError);

                        jwtData = {
                            id: user.id,
                            role: user.role,
                            restaurant_id: user.restaurant_id
                        };
                        return user.verifyPassword(args.password);
                    }).then(isMatch => {
                        if (!isMatch)
                            return callback(new WrongCredentialsError);

                        const token = JWT.sign(jwtData, config.secret);
                        return resolve({
                            token: token,
                            role: jwtData.role,
                            id: jwtData.id,
                            restaurant_id: jwtData.restaurant_id
                        });
                    }).catch(err => {
                        return callback(err);
                    });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    static quitRestaurant(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['normal'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    models.order.findAll({
                        client_id: auth.id
                    })
                        .then(orders => {
                            let unpayedOrders = [];
                            orders.map(order => {
                                if (order.payed === false)
                                    unpayedOrders.push(order);
                            });

                            if (unpayedOrders.length > 0)
                                return callback(new Error('You have to pay your orders first'));

                            return connectors.user.find.load(auth.id);
                        })
                        .then(user => {
                            return resolve(user.update({
                                restaurant_id: null
                            }));
                        })
                        .catch(err => {
                            return callback(err);
                        });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    static checkEmployer(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            connectors.user.getEmployer.load(args.id)
                .then(employer => {
                    return auth.id === employer.id ? resolve() : reject(new UnauthorizedError);
                })
                .catch(err => {
                    return reject(err);
                })
        })
    }

    static get(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth('*', auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    if (!Security.isUndefined(args.id)) {
                        if (auth.role !== 'owner')
                            return callback(new UnauthorizedError);

                        User.checkEmployer(args, connectors, auth)
                            .then(() => {
                                return resolve(connectors.user.find.load(args.id));
                            })
                            .catch(err => {
                                return callback(err);
                            });
                    } else
                        return resolve(connectors.user.find.load(auth.id));
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    static getOrders(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['normal', 'owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    connectors.user.getOrders.load(args.id || auth.id)
                        .then(orders => {
                            return resolve(orders);
                        })
                        .catch(err => {
                            return callback(err);
                        });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve(err);
            });
        });
    }

    static update(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['normal', 'owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    if (!Security.isUndefined(args.id)) {
                        if (auth.role !== 'owner')
                            return callback(new UnauthorizedError);

                        User.checkEmployer(args, connectors, auth)
                            .then(() => {
                                return callback();
                            })
                            .catch(err => {
                                return callback(err);
                            });
                    } else
                        return callback();
                },
                callback => {
                    connectors.user.find.load(auth.id)
                        .then(user => {
                            return user.verifyPassword(args.password_confirmation);
                        })
                        .then(isMatch => {
                            if (!isMatch)
                                return reject(new UnauthorizedError);

                            return !Security.isUndefined(args.id) ? connectors.user.find.load(args.id) : connectors.user.find.load(auth.id);
                        })
                        .then(user => {
                            delete args['id'];
                            delete args['password_confirmation'];
                            return resolve(user.update(args));
                        });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }
}

module.exports = User;