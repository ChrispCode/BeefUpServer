const async = require('async');

const models = require('../../db/models');
const stripe = require('../../config/stripe');
const pusher = require('../../config/pusher');
const Security = require('../security');
const Reservation = require('./reservation');

const {UnauthorizedError, SecurityUnauthorizedArgumentError, SecurityMissingArgumentError, SecurityInvalidArgumentError, WrongCredentialsError} = require('../errors');

class Restaurant {
    static create(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    return resolve(models.restaurant.create(Object.assign(args, {
                        owner_id: auth.id
                    })));
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    static get(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth('*', auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    return !Security.isUndefined(args.id) ? resolve(connectors.restaurant.find.load(args.id)) : resolve(connectors.restaurant.findAll());
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    static getClients(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Restaurant.checkOwner(args, connectors, auth)
                        .then(() => {
                            return resolve(connectors.restaurant.getClients.load(args.id));
                        })
                        .catch(err => {
                            return callback(err);
                        })
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    static getOrders(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (Security.checkAuth(['normal'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    if (auth.role === 'owner')
                        Restaurant.checkOwner(args, connectors, auth)
                            .then(() => {
                                return callback();
                            })
                            .catch(err => {
                                return callback(err);
                            });
                    else
                        connectors.user.find.load(auth.id)
                            .then(user => {
                                if (user.restaurant_id !== args.id)
                                    return callback(new UnauthorizedError);

                                return callback();
                            })
                            .catch(err => {
                                return callback(err)
                            })
                },
                callback => {
                    connectors.restaurant.getClients.load(args.id)
                        .then(clients => {
                            if (Security.isNull(clients))
                                return resolve([]);
                            const clientsIds = [];
                            Promise.all(clients.map(client => clientsIds.push(client.id)));
                            return connectors.user.getOrders.loadMany(clientsIds);
                        })
                        .then(orders => {
                            return resolve(...orders)
                        })
                        .catch(err => {
                            return callback(err);
                        });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    static getStaff(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Restaurant.checkOwner(args, connectors, auth)
                        .then(() => {
                            return resolve(connectors.restaurant.getStaff.load(args.id));
                        })
                        .catch(err => {
                            return callback(err);
                        })
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    static checkOwner(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            connectors.restaurant.find.load(args.id)
                .then(restaurant => {
                    return connectors.user.find.load(restaurant.owner_id);
                })
                .then(owner => {
                    return auth.id === owner.id ? resolve() : reject(new UnauthorizedError);
                })
                .catch(err => {
                    return reject(err);
                })
        });
    }

    static update(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Restaurant.checkOwner(args, connectors, auth)
                        .then(() => {
                            return connectors.restaurant.find.load(args.id);
                        })
                        .then(restaurant => {
                            delete args['id'];
                            return resolve(restaurant.update(args));
                        })
                        .catch(err => {
                            return callback(err);
                        })
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    static remove(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Restaurant.checkOwner(args, connectors, auth)
                        .then(() => {
                            return connectors.restaurant.find.load(args.id);
                        })
                        .then(restaurant => {
                            return resolve(restaurant.destroy());
                        })
                        .catch(err => {
                            return callback(err);
                        })
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            })
        });
    }

    static authorizeClient(args, connectors, auth) {
        const requestNumberOfPeople = args.number_of_people || 4;
        const requestDate = new Date();
        const requestDuration = new Date(requestDate);
        requestDuration.setMinutes(requestDuration.getMinutes() + (args.duration || 120) + 5);

        return new Promise((resolve, reject) => {
            async.waterfall([
                callback => {
                    if (!Security.checkAuth(['waiter', 'barman', 'cashier'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    connectors.user.find.load(auth.id)
                        .then(user => {
                            return callback(null, user)
                        })
                        .catch(err => {
                            return callback(err);
                        })
                },
                (user, callback) => {
                    connectors.user.find.load(args.client_id)
                        .then(client => {
                            if (!Security.isNull(client.restaurant_id))
                                return callback(new Error('This client is already authorized'));

                            return connectors.restaurant.getHalls.load(user.restaurant_id)
                                .then(halls => {
                                    const hallsIds = [];
                                    Promise.all(halls.map(hall => hallsIds.push(hall.id)));
                                    return connectors.hall.getTables.loadMany(hallsIds);
                                })
                                .then(hallsTables => {
                                    const tables = [];
                                    hallsTables.map(hallTables => {
                                        hallTables.map(table => {
                                            if (table.number_of_people >= requestNumberOfPeople)
                                                tables.push(table)
                                        })
                                    });

                                    if (tables.length === 0)
                                        return callback(new Error(`There is no table on which can sit ${requestNumberOfPeople} people`));

                                    tables.sort((table1, table2) => table1.number_of_people > table2.number_of_people);

                                    const tablesIds = [];
                                    tables.map(table => {
                                        tablesIds.push(table.id);
                                    });

                                    return callback(null, user, tables, tablesIds);
                                })
                                .catch(err => {
                                    return callback(err);
                                });
                        })
                        .catch(err => {
                            return callback(err);
                        });
                },
                (user, tables, tablesIds, callback) => {
                    let bestMatchTable = null;

                    connectors.table.getReservations.loadMany(tablesIds)
                        .then(tablesReservations => {
                            let bestMatch = null;

                            for (let i = 0; i < tablesReservations.length; i++) {
                                if (!Security.isNull(tablesReservations[i])) {
                                    let conflict = false;
                                    tablesReservations[i].map(reservation => {
                                        const currentReservationDateEnd = new Date(reservation.date);
                                        currentReservationDateEnd.setMinutes(currentReservationDateEnd.getMinutes() + reservation.duration + 5);
                                        if (Reservation.checkRange(requestDate, requestDuration, reservation.date) ||
                                            Reservation.checkRange(requestDate, requestDuration, currentReservationDateEnd))
                                            conflict = reservation;
                                    });
                                    if (!conflict) {
                                        bestMatch = tablesIds[i];
                                    }
                                }
                                else {
                                    if (!Security.isNull(tables[i].client_id))
                                        bestMatch = null;
                                    else
                                        bestMatch = tablesIds[i];
                                }

                                if (!Security.isNull(bestMatch))
                                    break;
                            }

                            if (Security.isNull(bestMatch)) // TODO: give some more info for the available ranges of time
                                return callback(new Error('There is no available table at this moment'));

                            return connectors.table.find.load(bestMatch);
                        })
                        .then(table => {
                            bestMatchTable = table;

                            return table.update({
                                client_id: args.client_id
                            });
                        })
                        .then(() => {
                            return connectors.user.find.load(args.client_id);
                        })
                        .then(client => {
                            pusher.trigger(`private-restaurants-client-${client.id}`, 'auth_client', {
                                date: new Date()
                            });

                            return client.update({
                                restaurant_id: user.restaurant_id
                            });
                        })
                        .then(() => {
                            return resolve(bestMatchTable);
                        })
                        .catch(err => {
                            return callback(err);
                        })
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            })
        });
    }

    static getBalance(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Restaurant.checkOwner(args, connectors, auth)
                        .then(() => {
                            return models.payment.findAll({
                                restaurant_id: args.id,
                                repaid: false
                            });
                        })
                        .then(payments => {
                            let amount = 0;
                            Promise.all(payments.map(payment => new Promise(resolve => {
                                amount += payment.amount;
                                return resolve(connectors.payment.find.prime(payment.id, payment));
                            })));
                            return resolve(amount);
                        })
                        .catch(err => {
                            return callback(err);
                        })
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    // static withrawBalance(args, connectors, auth) {
    //     return new Promise((resolve, reject) => {
    //         async.series([
    //             callback => {
    //                 if (!Security.checkAuth(['owner'], auth))
    //                     return callback(new UnauthorizedError);
    //
    //                 return callback();
    //             },
    //             callback => {
    //                 let amount = 0;
    //                 const foundPayments = [];
    //
    //                 Restaurant.checkOwner(args, connectors, auth)
    //                     .then(() => {
    //                         return models.payment.findAll({
    //                             restaurant_id: args.id,
    //                             repaid: false,
    //                             // where: {
    //                             //     created_at: {
    //                             //         gte: args.startDate,
    //                             //         lte: args.endDate
    //                             //     }
    //                             // }
    //                         });
    //                     })
    //                     .then(payments => {
    //                         Promise.all(payments.map(payment => new Promise(resolve => {
    //                             amount += payment.amount;
    //                             foundPayments.push(payment);
    //                             return resolve();
    //                         })));
    //
    //                         return connectors.restaurant.find.load(args.id);
    //                     })
    //                     .then(restaurant => {
    //                         return connectors.user.find.load(restaurant.owner_id);
    //                     })
    //                     .then(owner => {
    //                         return stripe.charges.create({
    //                             amount,
    //                             currency: 'bgn',
    //                             source: owner.stripe_account_id,
    //                             // customer: owner.stripe_account_id
    //                         });
    //                     })
    //                     .then(charge => {
    //                         Promise.all(foundPayments.map(payment => new Promise(resolve => {
    //                              return resolve(payment.update({
    //                                  repaid: true
    //                              }));
    //                         })));
    //                         return resolve('Success!');
    //                     })
    //                     .catch(err => {
    //                         return callback(err);
    //                     })
    //             }
    //         ], err => {
    //             if (err)
    //                 return reject(err);
    //
    //             return resolve();
    //         });
    //     });
    // }
}

module.exports = Restaurant;