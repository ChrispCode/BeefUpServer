const async = require('async');

const models = require('../../db/models');
const Security = require('../security');
const Restaurant = require('./restaurant');

const { UnauthorizedError, SecurityUnauthorizedArgumentError, SecurityMissingArgumentError, SecurityInvalidArgumentError, WrongCredentialsError } = require('../errors');

class Item {
    static create(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Restaurant.checkOwner({ id: args.restaurant_id }, connectors, auth)
                        .then(() => {
                            return resolve(models.item.create(args));
                        })
                        .catch(err => {
                            return callback(err);
                        });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            })
        });
    }

    static checkOwner(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            connectors.item.find.load(args.id)
                .then(item => {
                    return resolve(Restaurant.checkOwner({id: item.restaurant_id}, connectors, auth));
                })
                .catch(err => {
                    return reject(err);
                })
        });
    }

    static update(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Item.checkOwner(args, connectors, auth)
                        .then(() => {
                            return connectors.item.find.load(args.id);
                        })
                        .then(item => {
                            delete args['id'];
                            return resolve(item.update(args));
                        })
                        .catch(err => {
                            return callback(err);
                        });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            })
        });
    }

    static remove(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Item.checkOwner(args, connectors, auth)
                        .then(() => {
                            return connectors.item.find.load(args.id);
                        })
                        .then(item => {
                            return resolve(item.destroy());
                        })
                        .catch(err => {
                            return callback(err);
                        });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            })
        });
    }
}

module.exports = Item;