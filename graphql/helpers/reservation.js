const async = require('async');

const models = require('../../db/models');
const Security = require('../security');

const {UnauthorizedError, SecurityUnauthorizedArgumentError, SecurityMissingArgumentError, SecurityInvalidArgumentError, WrongCredentialsError} = require('../errors');

class Reservation {
    static create(args, connectors, auth) { // TODO: checks if at this moment the table is taken (add duration to table)
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['normal', 'waiter', 'barman', 'cashier'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    connectors.table.getReservations.load(args.table_id)
                        .then(reservations => {
                            const wantedReservationDateBegining = new Date(args.date);
                            const wantedReservationDateEnd = new Date(wantedReservationDateBegining);
                            wantedReservationDateEnd.setMinutes(wantedReservationDateEnd.getMinutes() + (args.duration || 120) + 5);
                            let conflict = false;
                            reservations.map(reservation => {
                                const currentReservationDateBegining = new Date(reservation.date);
                                const currentReservationDateEnd = new Date(currentReservationDateBegining);
                                currentReservationDateEnd.setMinutes(currentReservationDateEnd.getMinutes() + reservation.duration + 5);
                                if (Reservation.checkRange(wantedReservationDateBegining, wantedReservationDateEnd, currentReservationDateBegining) || Reservation.checkRange(wantedReservationDateBegining, wantedReservationDateEnd, currentReservationDateEnd))
                                    conflict = reservation;
                            });

                            if (conflict) // TODO: give some more information about the time that is free on that table
                                return callback(new Error('There is already reservation at this time on this table'));

                            return callback();
                        })
                        .catch(err => {
                            return callback(err);
                        });
                },
                callback => {
                    if (auth.role === 'normal') {
                        return resolve(models.reservation.create({
                            client_id: auth.id,
                            date: args.date,
                            duration: args.duration || 120,
                            table_id: args.table_id
                        }));
                    } else {
                        // TODO: when a client without account wants to make a reservation
                    }
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            })
        });
    }

    static checkRange(begining, end, value) {
        begining = new Date(begining).getTime();
        end = new Date(end).getTime();
        value = new Date(value).getTime();
        return value >= begining && value <= end;
    }

    static checkOwner(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            if (auth.role === 'normal') {
                connectors.reservation.find.load(args.id)
                    .then(reservation => {
                       return auth.id === reservation.client_id ? resolve(): reject(new UnauthorizedError);
                    })
                    .catch(err => {
                        return reject(err);
                    });
            } else {
                let restaurantId;
                connectors.reservation.find.load(args.id)
                    .then(reservation => {
                        return connectors.table.find.load(reservation.table_id);
                    })
                    .then(table => {
                        return connectors.hall.find.load(table.hall_id);
                    })
                    .then(hall => {
                        restaurantId = hall.restaurant_id;
                        return connectors.user.find.load(auth.id);
                    })
                    .then(user => {
                        return user.restaurant_id === hall.restaurant_id ? resolve(): reject(new UnauthorizedError);
                    })
                    .catch(err => {
                        return reject(err);
                    });
            }
        });
    }

    static remove(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.waterfall([
                callback => {
                    if (!Security.checkAuth(['normal', 'waiter', 'barman', 'cashier'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    connectors.reservation.find.load(args.id)
                        .then(reservation => {
                            return callback(null, reservation);
                        })
                        .catch(err => {
                            return callback(err);
                        });
                },
                (reservation, callback) => {
                    Reservation.checkOwner(args, connectors, auth)
                        .then(() => {
                            return resolve(reservation.destroy());
                        })
                        .catch(err => {
                            return reject(err);
                        })
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            })
        });
    }
}

module.exports = Reservation;