const async = require('async');

const models = require('../../db/models');
const stripe = require('../../config/stripe');
const pusher = require('../../config/pusher');
const Security = require('../security');
const Restaurant = require('./restaurant');

const {UnauthorizedError, SecurityUnauthorizedArgumentError, SecurityMissingArgumentError, SecurityInvalidArgumentError, WrongCredentialsError} = require('../errors');

class Order {
    static create(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.waterfall([
                callback => {
                    if (!Security.checkAuth(['normal', 'waiter', 'barman'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    connectors.user.find.load(auth.id)
                        .then(user => {
                            if (Security.isUndefined(user.restaurant_id))
                                return callback(new UnauthorizedError);

                            return callback(null, user);
                        })
                        .catch(err => {
                            return callback(err);
                        });
                },
                (user, callback) => {
                    let itemsIds = [];
                    let itemsQuantities = [];
                    args.items.map(item => {
                        itemsIds.push(item.item_id);
                        itemsQuantities[item.item_id] = item.quantity;
                    });
                    connectors.item.find.loadMany(itemsIds)
                        .then(items => {
                            if (items.indexOf(null) > -1)
                                return callback(new Error('Cannot find the items'));

                            items.map(item => {
                                if (item.restaurant_id !== user.restaurant_id)
                                    return callback(new Error('You should be authorized in the restaurant'));

                                item.order_item = {
                                    quantity: itemsQuantities[item.id]
                                };
                            });

                            return callback(null, items, user);
                        })
                        .catch(err => {
                            return callback(err);
                        });
                },
                (items, user, callback) => {
                    let createdOrder;
                    ['waiter', 'barman'].indexOf(auth.role) > -1 ? models.order.create() : models.order.create({
                        client_id: auth.id
                    }).then(order => {
                        createdOrder = order;
                        return order.addItems(items);
                    })
                    .then(items => {
                        pusher.trigger(`private-restaurants-${user.restaurant_id}`, 'new_order', {
                            date: new Date()
                        });

                        return resolve(createdOrder);
                    })
                    .catch(err => {
                        return callback(err);
                    })
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            })
        });
    }

    static pay(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.waterfall([
                callback => {
                    if (!Security.checkAuth(['normal', 'waiter', 'barman'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    connectors.order.find.load(args.id)
                        .then(order => {
                            if (order.payed)
                                return callback(new Error('This order is already paid'));
                            if (order.client_id !== auth.id)
                                return callback(new UnauthorizedError);

                            return callback(null, order);
                        })
                        .catch(err => {
                            return callback(err);
                        });
                },
                (order, callback) => {
                    if (args.payment_method === 'card') {
                        if (auth.role !== 'normal')
                            return callback(new Error('This order can\'t be paid with card'));

                        let amount = 0;
                        let orderItems = {};
                        let foundUser;
                        let foundRestaurantId;

                        connectors.user.find.load(auth.id)
                            .then(user => {
                                foundUser = user;
                                if (Security.isNull(user.stripe_customer_id))
                                    return callback(new Error('You don\'t have registered card'));
                                else
                                    return connectors.order.getItems.load(order.id);
                            })
                            .then(items => {
                                const itemsIds = [];
                                Promise.all(items.map(item => new Promise(resolve => {
                                    orderItems[item.item_id] = item;
                                    itemsIds.push(item.item_id);
                                    return resolve();
                                })));
                                return connectors.item.find.loadMany(itemsIds);
                            })
                            .then(items => {
                                Promise.all(items.map(item => new Promise(resolve => {
                                    amount += item.price * orderItems[item.id].quantity;
                                    return resolve();
                                })));

                                foundRestaurantId = items[0].restaurant_id;

                                return models.user.findOne({
                                    where: {
                                        restaurant_id: foundRestaurantId,
                                        role: 'owner'
                                    }
                                });
                            })
                            .then(owner => {
                                return stripe.charges.create({
                                    amount,
                                    currency: 'bgn',
                                    customer: foundUser.stripe_customer_id,
                                    destination: {
                                        account: owner.stripe_account_id
                                    }
                                });
                            })
                            .then(charge => {
                                return order.createPayment({
                                    stripe_charge_id: charge.id,
                                    amount: charge.amount,
                                    restaurant_id: foundRestaurantId
                                });
                            })
                            .then(payment => {
                                return callback(null, order);
                            })
                            .catch(err => {
                                return callback(err);
                            });
                    } else
                        return callback(null, order);
                },
                (order, callback) => {
                    return resolve(order.update({
                        payed: true
                    }));
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            })
        });
    }
}

module.exports = Order;