const async = require('async');

const models = require('../../db/models');
const Security = require('../security');
const Hall = require('./hall');

const { UnauthorizedError, SecurityUnauthorizedArgumentError, SecurityMissingArgumentError, SecurityInvalidArgumentError, WrongCredentialsError } = require('../errors');

class Table {
    static create(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Hall.checkOwner({ id: args.hall_id }, connectors, auth)
                        .then(() => {
                            return resolve(models.table.create(args));
                        })
                        .catch(err => {
                            return callback(err);
                        });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            })
        });
    }

    static checkOwner(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            connectors.table.find.load(args.id)
                .then(table => {
                    return resolve(Hall.checkOwner({ id: table.hall_id }, connectors, auth));
                })
                .catch(err => {
                    return reject(err);
                })
        });
    }

    static getClients(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Table.checkOwner(args, connectors, auth)
                        .then(() => {
                            return resolve(connectors.table.getClients.load(args.id));
                        })
                        .catch(err => {
                            return callback(err);
                        });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    static getOrders(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Table.checkOwner(args, connectors, auth)
                        .then(() => {
                            return resolve(connectors.table.getOrders.load(args.id));
                        })
                        .catch(err => {
                            return callback(err);
                        });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            });
        });
    }

    static update(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Table.checkOwner(args, connectors, auth)
                        .then(() => {
                            return connectors.table.find.load(args.id);
                        })
                        .then(table => {
                            delete args['id'];
                            return resolve(table.update(args));
                        })
                        .catch(err => {
                            return callback(err);
                        });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            })
        });
    }

    static remove(args, connectors, auth) {
        return new Promise((resolve, reject) => {
            async.series([
                callback => {
                    if (!Security.checkAuth(['owner'], auth))
                        return callback(new UnauthorizedError);

                    return callback();
                },
                callback => {
                    Table.checkOwner(args, connectors, auth)
                        .then(() => {
                            return connectors.table.find.load(args.id);
                        })
                        .then(table => {
                            return resolve(table.destroy());
                        })
                        .catch(err => {
                            return callback(err);
                        });
                }
            ], err => {
                if (err)
                    return reject(err);

                return resolve();
            })
        });
    }
}

module.exports = Table;