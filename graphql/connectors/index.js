const models = require('../../db/models');
const DataLoader = require('dataloader');
const {UnauthorizedError} = require('../errors');
const Security = require('../security');
let newConnectors = {};

const fixResults = (ids, items, param) => new Promise(resolve => {
    const data = [];
    Promise.all(ids.map(id => new Promise(resolve => {
        let found = [];
        items.map(item => {
            if (item[param] === id || item.id === id) {
                found.push(item);
            }
        });
        if (found.length > 0)
            data.push(found);
        else
            data.push(null);

        return resolve();
    })));

    return resolve(data);
});

const connectors = {
    user: {
        find: ids => models.user.findAll({
            where: {
                id: ids
            }
        }),
        restaurant: ids => newConnectors.user.find.load(ids)
            .then(user => {
                return newConnectors.restaurant.find.load(user.restaurant_id);
            }),
        getEmployer: ids => newConnectors.user.find.load(...ids)
            .then(user => {
                if (Security.isUndefined(user) || ['waiter', 'cook', 'barman', 'cashier'].indexOf(user.role) == -1)
                    throw new UnauthorizedError;

                return newConnectors.restaurant.find.load(user.restaurant_id);
            })
            .then(restaurant => {
                return newConnectors.user.find.load(restaurant.owner_id);
            }),
        getOrders: ids => new Promise(resolve => {
            models.order.findAll({
                where: {
                    client_id: ids
                }
            }).then(orders => {
                Promise.all(orders.map(order => newConnectors.order.find.prime(order.id, order)));
                return resolve(fixResults(ids, orders, 'client_id'));
            });
        })
    },
    restaurant: {
        find: ids => models.restaurant.findAll({
            where: {
                id: ids
            }
        }),
        getClients: ids => new Promise(resolve => {
            models.user.findAll({
                where: {
                    restaurant_id: ids,
                    role: 'normal'
                }
            }).then(clients => {
                Promise.all(clients.map(client => newConnectors.user.find.prime(client.id, client)));
                return resolve(fixResults(ids, clients));
            });
        }),
        getStaff: ids => new Promise(resolve => {
            models.user.findAll({
                where: {
                    restaurant_id: ids,
                    role: ['waiter', 'barman', 'cook', 'cashier']
                }
            }).then(staff => {
                Promise.all(staff.map(member => newConnectors.user.find.prime(member.id, member)));
                return resolve(fixResults(ids, staff, 'restaurant_id'));
            });
        }),
        getHalls: ids => new Promise(resolve => {
            models.hall.findAll({
                where: {
                    restaurant_id: ids
                }
            }).then(halls => {
                Promise.all(halls.map(hall => newConnectors.hall.find.prime(hall.id, hall)));
                return resolve(fixResults(ids, halls));
            })
        }),
        getItems: ids => new Promise(resolve => {
            models.item.findAll({
                where: {
                    restaurant_id: ids
                }
            }).then(items => {
                Promise.all(items.map(item => newConnectors.item.find.prime(item.id, item)));
                return resolve(fixResults(ids, items, 'restaurant_id'));
            })
        })
    },
    order: {
        find: ids => models.order.findAll({
            where: {
                id: ids
            }
        }),
        getClients: ids => new Promise(resolve => {
            newConnectors.order.find.loadMany(ids)
                .then(orders => {
                    const clientsIds = [];
                    Promise.all(orders.map(order => clientsIds.push(order.client_id)));
                    return newConnectors.user.find.loadMany(clientsIds);
                })
                .then(clients => {
                    return resolve(clients)
                });
        }),
        getTables: ids => new Promise(resolve => {
            newConnectors.order.find.loadMany(ids)
                .then(orders => {
                    const tablesIds = [];
                    Promise.all(orders.map(order => tablesIds.push(order.table_id)));
                    return newConnectors.table.find.loadMany(tablesIds);
                })
                .then(tables => {
                    return resolve(tables);
                });
        }),
        getItems: ids => new Promise(resolve => {
            models.order_item.findAll({
                where: {
                    order_id: ids
                }
            }).then(items => {
                Promise.all(items.map(item => newConnectors.item.find.prime(item.id, item)));
                return resolve(fixResults(ids, items, 'order_id'));
            })
        })
    },
    hall: {
        find: ids => models.hall.findAll({
            where: {
                id: ids
            }
        }),
        getTables: ids => new Promise(resolve => {
            models.table.findAll({
                where: {
                    hall_id: ids
                }
            }).then(tables => {
                Promise.all(tables.map(table => newConnectors.table.find.prime(table.id, table)));
                return resolve(fixResults(ids, tables, 'hall_id'));
            });
        }),
        getRestaurant: ids => new Promise(resolve => {
            newConnectors.hall.find.loadMany(ids)
                .then(halls => {
                    const restaurantsIds = [];
                    Promise.all(halls.map(hall => restaurantsIds.push(hall.restaurant_id)));
                    return newConnectors.restaurant.find.loadMany(restaurantsIds);
                })
                .then(restaurants => {
                    return resolve(restaurants);
                });
        })
    },
    table: {
        find: ids => models.table.findAll({
            where: {
                id: ids
            }
        }),
        getHalls: ids => new Promise(resolve => {
            newConnectors.table.find.loadMany(ids)
                .then(tables => {
                    const hallsIds = [];
                    Promise.all(tables.map(table => hallsIds.push(table.hall_id)));
                    return newConnectors.hall.find.loadMany(hallsIds);
                })
                .then(tables => {
                    return resolve(tables);
                });
        }),
        getClients: ids => new Promise(resolve => {
            newConnectors.table.find.loadMany(ids)
                .then(tables => {
                    const clientsIds = [];
                    Promise.all(tables.map(table => clientsIds.push(table.client_id)));
                    // TODO if there aren'y any clients should return null
                    return newConnectors.user.find.loadMany(clientsIds);
                })
                .then(clients => {
                    return resolve(clients)
                });
        }),
        getOrders: ids => new Promise(resolve => {
            newConnectors.table.getClients.loadMany(ids)
                .then(clients => {
                    const orders = [];
                    Promise.all(clients.map(client => new Promise(resolve => {
                        newConnectors.user.getOrders.load(client.id)
                            .then(clientOrders => {
                                orders.push(clientOrders);
                                return resolve();
                            })
                    }))).then(() => {
                        return resolve(orders);
                    });

                });
        }),
        getReservations: ids => new Promise(resolve => {
            models.reservation.findAll({
                where: {
                    table_id: ids
                }
            }).then(reservations => {
                Promise.all(reservations.map(reservation => newConnectors.reservation.find.prime(reservation.id, reservation)));
                return resolve(fixResults(ids, reservations, 'table_id'));
            });
        })
    },
    item: {
        find: ids => models.item.findAll({
            where: {
                id: ids
            }
        })
    },
    reservation: {
        find: ids => models.reservation.findAll({
            where: {
                id: ids
            }
        })
    },
    payment: {
        find: ids => models.payment.findAll({
            where: {
                id: ids
            }
        })
    }
};

module.exports = () => {
    newConnectors = {
        restaurant: {
            findAll: () => new Promise(resolve => models.restaurant.findAll().then(restaurants => {
                Promise.all(restaurants.map(restaurant => newConnectors.restaurant.find.prime(restaurant.id, restaurant)));
                return resolve(restaurants);
            }))
        }
    };
    for (let connector in connectors) {
        for (let method in connectors[connector]) {
            const loader = new DataLoader(ids => new Promise((resolve, reject) => {
                    // console.log(JSON.stringify(ids))
                    connectors[connector][method](ids)
                        .then(items => {
                            // if (items.length == 0)
                            //     return reject(new Error('There is no such ' + connector));

                            return resolve(items);
                        })
                        .catch(err => {
                            return reject(err);
                        });
                })
            );

            if (!newConnectors[connector])
                newConnectors[connector] = {};

            newConnectors[connector][method] = loader;
        }
    }

    return newConnectors;
};