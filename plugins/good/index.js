const config = require('../../config');

const compression = config => {
    let c = {
        compress: config.good.compress,
        size: config.good.size,
        path: config.good.path
    };

    if (!c.compress)
        delete c.compress;
    else if (config.good.type)
        c.compress = config.good.type;

    return c;
};

const options = {
    ops: {
        interval: 5000
    },
    reporters: {
        consoleReporter: [{
            module: 'good-squeeze',
            name: 'Squeeze',
            args: [{ log: '*', response: '*' }]
        }, {
            module: 'good-console'
        }, 'stdout'],
        opsReporter: [{
            module: 'good-squeeze',
            name: 'Squeeze',
            args: [{ ops: '*' }]
        }, {
            module: 'good-squeeze',
            name: 'SafeJson',
            args: [
                null,
                { separator: ',' }
            ]
        }, {
            module: 'rotating-file-stream',
            args: [
                'ops.log',
                compression(config)
            ]
        }],
        errorReporter: [{
            module: 'good-squeeze',
            name: 'Squeeze',
            args: [{ error: '*' }]
        }, {
            module: 'good-squeeze',
            name: 'SafeJson',
            args: [
                null,
                { separator: ',' }
            ]
        }, {
            module: 'rotating-file-stream',
            args: [
                'err.log',
                compression(config)
            ]
        }]
    }
};

module.exports = {
    register: require('good'),
    options: options
};