const schema = require('../../graphql/schema');
const { graphqlHapi } = require('graphql-server-hapi');
const formatError = require('./formatError');
const connectors = require('../../graphql/connectors');

module.exports = {
    register: graphqlHapi,
    options: {
        path: '/graphql',
        graphqlOptions: request => {
            return {
                schema: schema,
                context: {
                    auth: request.auth,
                    connectors: connectors()
                }
            }
        }
    }
};